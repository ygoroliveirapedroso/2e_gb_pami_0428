import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'detalhes',
    loadChildren: () => import('./detalhes/detalhes.module').then( m => m.DetalhesPageModule)
  },
  {
    path: 'cad-autor',
    loadChildren: () => import('./cad-autor/cad-autor.module').then( m => m.CadAutorPageModule)
  },
  {
    path: 'cad-livro',
    loadChildren: () => import('./cad-livro/cad-livro.module').then( m => m.CadLivroPageModule)
  },
  {
    path: 'cad-editora',
    loadChildren: () => import('./cad-editora/cad-editora.module').then( m => m.CadEditoraPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
